#!/usr/bin/env python
import os.path
import sys
import gfal2

if(len(sys.argv) < 2):
    print("Usage %s [URL]"%(sys.argv[0]))
    exit(1)

ctxt = gfal2.creat_context()
try:
    stat = ctxt.stat("file://" + sys.argv[1])
except:
    print("could not stat %s" % sys.argv[1])
    sys.exit(1)

try:
    md5 = ctxt.checksum("file://" + sys.argv[1], 'MD5')
except:
    print("could not md5 %s" % sys.argv[1])
    sys.exit(1)

try:
    adler32 = ctxt.checksum("file://" + sys.argv[1], 'Adler32')
except:
    print("could not Adler32 %s" % sys.argv[1])
    sys.exit(1)

if not stat or not md5 or not adler32:
    print("GFAL failures for %s" % sys.argv[1])
    sys.exit(1)

print("%s %d %s %s"%(os.path.basename(sys.argv[1]), stat.st_size, adler32, md5))
