FROM centos:7
ARG BUILD_DATE
ARG CI_COMMIT_SHA

RUN yum update -y &&\
      rpm -ivh https://repo.opensciencegrid.org/osg/3.5/osg-3.5-el7-release-latest.rpm && \
      yum install -y osg-ca-certs epel-release

RUN yum install -y gfal2-util gfal2-all && \
      yum clean all && \
      rm -rf /var/cache/yum

COPY entrypoint/gfal-copy.sh /gfal-copy.sh
ENTRYPOINT ["/gfal-copy.sh"]
CMD ["/bin/bash"]
