#!/usr/bin/env python
#
"""Recursively list a directory with gfal2.  Reproduced from
https://github.com/cern-fts/gfal2-python/blob/master/example/python/gfal2_recursive_ls.py
"""
# pylint: disable=too-few-public-methods
import argparse
import stat
import sys
from datetime import datetime
import gfal2


def _short_format(fname):
    """Return name in normal short format"""
    return fname


def _modestr(mode):
    """Get mode of file"""
    string = ['-'] * 10
    if stat.S_ISDIR(mode):
        string[0] = 'd'
    elif stat.S_ISLNK(mode):
        string[0] = 'l'
    elif not stat.S_ISREG(mode):
        string[0] = '?'

    for i in range(3):
        if mode & stat.S_IRUSR:
            string[1 + i * 3] = 'r'
        if mode & stat.S_IWUSR:
            string[2 + i * 3] = 'w'
        if mode & stat.S_IXUSR:
            string[3 + i * 3] = 'x'
        mode = mode << 3
    return ''.join(string)


def _long_format(fname, fstat):
    """Return long format with stat of file"""
    return "%s %3d %5d %5d %10d %s %s" % \
        (_modestr(fstat.st_mode),
         fstat.st_nlink, fstat.st_uid, fstat.st_gid,
         fstat.st_size,
         datetime.fromtimestamp(fstat.st_mtime).strftime('%b %d %H:%M'),
         fname)


class Crawler:
    """Crawl directories with gfal2"""

    def __init__(self, long=False, recursive=False, files=False,
                 max_levels=2):

        self.context = gfal2.creat_context()
        self.long = long
        self.recursive = recursive
        self.files = files
        self.max_levels = max_levels

    def _crawl(self, url, out, level=0):
        if level > self.max_levels:
            return

        tabbing = '  ' * level
        try:
            entries = [fstr for fstr in self.context.listdir(url)
                       if fstr not in ('.', '..')]
        except gfal2.GError:
            out.write(tabbing, '!')
            return

        for fstr in entries:
            full = url + '/' + fstr
            # Do the stat only if we need to
            if self.recursive or self.long:
                try:
                    fstat = self.context.stat(full)
                except gfal2.GError:
                    fstat = self.context.st_stat()

            # Print entry
            if self.files:
                # Only print if not a directory so we CANNOT list it
                try:
                    self.context.listdir(full)
                except gfal2.GError:
                    if self.long:
                        out.write(_long_format(fstr, fstat) + '\n')
                    else:
                        out.write(_short_format(fstr) + '\n')
            else:
                if self.long:
                    out.write(tabbing + _long_format(fstr, fstat) + '\n')
                else:
                    out.write(tabbing + _short_format(fstr) + '\n')

            # Descend
            if self.recursive and stat.S_ISDIR(fstat.st_mode):
                self._crawl(full, out, level + 1)

    def crawl(self, url, out=sys.stdout):
        """Call crawler"""
        self._crawl(url, out)


if __name__ == '__main__':
    # Get the options
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(dest='url', help='URL to crawl', nargs=1)
    parser.add_argument('-l', '--long', dest='long',
                        default=False, action='store_true',
                        help='Long format')
    parser.add_argument('-r', '--recursive', dest='recursive',
                        default=False, action='store_true',
                        help='Recursive')
    parser.add_argument('-m', '--max', dest='max_recursive',
                        default=1, type=int,
                        help='Maximum recursive level')
    parser.add_argument('-f', '--files', dest='files',
                        default=False, action='store_true',
                        help='Only show files')

    options = parser.parse_args(sys.argv[1:])

    # List recursively
    crawler = Crawler(options.long, options.recursive, options.files,
                      options.max_recursive)

    crawler.crawl(options.url[0], sys.stdout)
