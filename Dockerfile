# gwrucio_registrar
# Rucio registration client for IGWN-curated data
FROM conda/miniconda3-centos7
ARG ENV
ARG BUILD_DATE
ARG CI_COMMIT_SHA
ARG RUCIO_VERSION

RUN yum update -y &&\
      rpm -ivh https://repo.opensciencegrid.org/osg/3.5/osg-3.5-el7-release-latest.rpm && \
      yum install -y osg-ca-certs && \
      yum clean all && \
      rm -rf /var/cache/yum

# http://label-schema.org/rc1/
LABEL org.label-schema.schema-version="1.0"
#LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.name="igwn-rucio-client"
LABEL org.label-schema.description="Rucio client utilities for International Gravitational Wave Network"
LABEL org.label-schema.url="https://git.ligo.org/rucio/igwn-rucio-client"
LABEL org.label-schema.vcs-url="https://git.ligo.org/rucio/igwn-rucio-client"
LABEL org.label-schema.build-date="${BUILD_DATE}"
LABEL org.label-schema.vcs-ref="${CI_COMMIT_SHA}"

# Initial Setup
COPY environment.yml /environment.yml
COPY entrypoint/rucio.cfg.j2 /rucio.cfg.j2

# Conda dependencies
RUN conda update -yq -nbase conda 
RUN conda env create -f /environment.yml 

# Pip dependencies
SHELL ["conda", "run", "-n", "gwrucio", "/bin/bash", "-c"]
RUN pip install --no-cache-dir rucio-clients==${RUCIO_VERSION} \
      igwn-rucio-lfn2pfn

# Install from pypi for production images
RUN if [ "$ENV" = "production" ] ; then \
      pip install --no-cache-dir gwrucio_registrar; \
      else \
      pip install --no-cache-dir pip install "git+https://git.ligo.org/rucio/igwn-rucio-client.git#egg=gwrucio_registrar&subdirectory=gwrucio_registrar" ;\
      fi

COPY entrypoint/bashrc /root/.bashrc
COPY entrypoint/docker-entrypoint.sh /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["/bin/bash"]
