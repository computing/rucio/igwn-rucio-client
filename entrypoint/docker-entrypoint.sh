#!/bin/sh

source $(conda info --base)/etc/profile.d/conda.sh
conda activate gwrucio

if [[ ! -f /opt/rucio/etc/rucio.cfg && ! -f ${RUCIO_HOME}/etc/rucio.cfg ]]; then
    echo "File rucio.cfg not found. It will generate one."
    mkdir -p /opt/rucio/etc/
    j2 rucio.cfg.j2 > /opt/rucio/etc/rucio.cfg
fi

exec "$@"
