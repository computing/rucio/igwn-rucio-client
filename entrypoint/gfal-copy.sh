#!/bin/bash -e
COPYCMD="/usr/bin/gfal-copy -f"

log_event(){
  event=$1
  log=$(printf '[%s %s %s]\n' "$(basename $0)" "$(date --rfc-3339=seconds)")
  echo "$log $event"
}

print_usage(){
  echo "Usage: SRC=/source DEST=/destination $(basename $0)"
  echo "SRC and DEST are environment variables:"
  echo -e "SRC \t source file (e.g. gsiftp://host:port/path)"
  echo -e "DST \t destination file(s). (e.g., /new_path)"
}


if [ -z "${SRC}" ]; then
  echo "Please set SRC"
  print_usage
  exit 1
fi
if [ -z "${DEST}" ]; then
  echo "Please set DEST"
  print_usage
  exit 1
fi
if [ -z "${MAX_RETRIES}" ]; then
  MAX_RETRIES=5
  echo "Using MAX_RETRIES=${MAX_RETRIES}"
fi

if [[ -z "${DAEMON}" || "${DAEMON}" != "True" && "${DAEMON}" != "true" ]]; then
  log_event "Not running in daemon mode"
  DAEMON="False"
else
  log_event "Running in daemon mode"
  DAEMON="True"
  if [ -z "${SLEEP_INTERVAL}" ]; then
    SLEEP_INTERVAL=5
  fi
  log_event "Setting sleep interval to ${SLEEP_INTERVAL}"
fi

while /bin/true; do

  retry=1
  nretry=1
  log_event "Fetching: ${SRC}"
  until ${COPYCMD} ${SRC} ${DEST}.tmp; do
    retry=$((${retry}*2))
    log_event "Trying again in ${retry} seconds ($nretry/$MAX_RETRIES)"

    if [ ${nretry} -ge ${MAX_RETRIES} ]; then
      echo "Maximum retries reached: connectivity issue"
      exit 1
    fi
    nretry=$((nretry+1))

    sleep ${retry}
  done

  if [ -s ${DEST}.tmp ]; then
    log_event "Available: ${DEST}"
    mv ${DEST}.tmp ${DEST}
  else
    log_event "${DEST}.tmp not found or is empty"
  fi

  if [ ${DAEMON} != "True" ]; then
    exit 0
  fi

  log_event "Going to sleep for ${SLEEP_INTERVAL} seconds"
  sleep ${SLEEP_INTERVAL}

done
